#!/usr/bin/env python

import sys

# Initialize counters to 0 in hash --
counts = {}

# For each word (arg) on the command line, separated by spaces --
for word in args:

    # Count based on first-letter of each word --
    case xxx

# Report the counts --
for name, count in counts.iteritems():
    sys.stdout.write( "Count of words beginning with a %s: %d\n" % ( name, count ) )

exit true
