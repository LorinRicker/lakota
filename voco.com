$ ! VOCO.COM -- DCL (OpenVMS) version
$ !
$ SPC = " "
$ words = "''P1' ''P2' ''P3' ''P4' ''P5' ''P6' ''P7' ''P8'"
$ vowels = 0
$ consonants = 0
$ digits = 0
$ !
$ j = 0
$loop:
$ word = F$ELEMENT( j, SPC, words )
$ IF ( word .EQS. SPC ) THEN GOTO Report
$ letter1 = F$EXTRACT( 0, 1, word )
$ IF ( F$LOCATE( letter1, "aeiou" ) .LT. 5 )
$ THEN vowels = vowels + 1
$      GOTO nextword
$ ENDIF
$ IF ( F$LOCATE( letter1, "0123456789" ) .LT. 10 )
$ THEN digits = digits + 1
$      GOTO nextword
$ ENDIF
$ ! None of the above? ...then it's a consonant
$ consonants = consonants + 1
$nextword:
$ j = j + 1
$ GOTO loop
$ !
$Report:
$ WRITE sys$output "Count of words beginning with a consonant: ''consonants'"
$ WRITE sys$output "Count of words beginning with a vowel: ''vowels'"
$ WRITE sys$output "Count of words beginning with a digit: ''digits'"
$ EXIT 1
$ !
