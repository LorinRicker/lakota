#!/usr/bin/env ruby
# -*- encoding: utf-8 -*-

# Initialize counters to 0 in hash --
counts = Hash.new( 0 )

# For each word (arg) on the command line, separated by spaces --
ARGV.each do | word |

  # Count based on first-letter of each word --
  case word[0]
  when /[aeiou]/
    counts[ :vowel ] += 1
  when /[0-9]/
    counts[ :digit ] += 1
  else
    counts[ :consonant ] += 1
  end  # case

end  # ARGV.each

# Report the counts --
counts.each_pair { | key, count |
  puts "Count of words beginning with a #{key}: #{count}"
}

exit true
