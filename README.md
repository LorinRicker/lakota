# On Learning to Read Code, and Learning to Program

## lakota

This is an essay, for beginners at programming/coding, on how to start learning to read code, how to compare languages to see their similarities, and how to learn to code.

The essay will provide illustrative, yet simple, code examples in several (a few) scripting languages, notably Ruby, Python, bash and DCL, in side-by-side comparisons.  Starting from the almost trite but obligatory "Hello, World", examples will actually "do something useful or real," rather than existing as simply academic samples of kind.

Intent is to address, and perhaps overcome, the "Which is better, Ruby or Python..." types of questions so often encountered on Quora.com and/or StackOverflow.com, which so often disguises the question "which programming language should I learn" (as if there should be only one).

### Copyright Notice and GNU General Public License (GPL v3.0)

Copyright (C) 2018, Lorin M. RIcker

These program and essay are free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
